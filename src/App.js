import React, { Component } from 'react';
import './main.css';

/*

  Давайте разберем React Component Lifecycle
  https://reactjs.org/docs/react-component.html#lifecycle-methods

*/

const style = {
  color: 'palevioletred',
  backgroundColor: 'papayawhip',
  padding: '5px 20px'
}

const Btn = ({ btnStyle, handler }) => {
  return(
      <button style={btnStyle}  onClick={handler}>CLICK</button>
      )

};

const ListItem = (props) => {
    let {interviewed, user} = props.user;
  return(
      <li className={interviewed ? "activi" : "dactive"}>{user.name}</li>

  )
};



class App extends Component {

  constructor( props ){
    super(props);
    this.state ={
        transformedArray:[]
    }
  }

  handleOnBtn = (event) => {
    console.log("GO")
  };


  componentDidMount() {

    let requestUrl = "http://www.json-generator.com/api/json/get/ckPDzNHDAO?indent=2";

    fetch(requestUrl).then(
        res => res.json()
    ).then(
        data => {

            let transformedArray = data.map(user => {
                return ({
                    interviewed: false,
                    user
                });
            });
            this.setState({transformedArray});
        }
    );
}

  render() {

    return (

      <div className="App">
      <h1>Statefull and Stateless components. Components with childrens</h1>
          <ul>
              {this.state.transformedArray.map((item, key) => {
                  return(
                      <ListItem
                      user={item}
                      key={key}
                      />
                  )
              })}
          </ul>

        <Btn
          btnStyle={style}
          handler={this.handleOnBtn}/>

      </div>
    );
  }
}


export default App;

// nameUser => {`${users.fname} + ${users.lname}`
// }
// const users = {
//   fname:"Andrey",
//   lname:"bill"
// };
// const element = (
//     <h1>Hello, {nameUser}</h1>
// );
// ReactDOM.render (
//     element
// );